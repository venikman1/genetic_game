﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace визуализатор
{
    public partial class Form1 : Form
    {
        //настройка дизайна объектов
        SolidBrush PlayerBrush = new SolidBrush(Color.Black);
        SolidBrush enemyBrush = new SolidBrush(Color.Red);
        SolidBrush playerBulletBrush = new SolidBrush(Color.Green);
        SolidBrush enemyBulletBrush = new SolidBrush(Color.Yellow);
        Color defaultColor = Color.White;

        //настройка параметров
        //настройка параметров игрока
        const int playerRadius = 5;
        static Point playerStartPosition = new Point(10, 10);
        Point playerPosition = playerStartPosition;
        bool playerBulletExist = false;
        Point playerBulletPosition;
        double PlayerAngle = 0;
        const int playerBulletRadius = 2;
        const int playerMoveSpeed = 10;
        const double playerRotateSpeed = 0.1;

        //настройка параметров врага
        const int enemyRadius = 5;
        static Point enemyStartPosition = new Point(1350, 750);
        Point enemyPosition = enemyStartPosition;
        bool enemyBulletExist = false;
        Point enemyBulletPosition;
        double enemyAngle = 0;
        const int enemyBulletRadius = 2;
        const int enemyMoveSpeed = 1;
        const double enemyRotateSpeed = 0.1;



        Rectangle Circle2Rectangle(Point center, int r)
        {
            return new Rectangle(center.X - r, center.Y - r, 2 * r, 2 * r);
        }

        Point getLastPoint(Point point) {
            return new Point(point.X + Width + 1, Width + point.Y + 1);
        }

        Point sumPoints(Point lhs, Point rhs) {
            return new Point(lhs.X + rhs.X, lhs.Y + rhs.Y);
        }

        void update() {
            
        }

        Graphics g;
        public Form1()
        {
            InitializeComponent();
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            paintPanel.Location = new Point(0, 0);
            paintPanel.Size = new Size(Width, Height);
            g = paintPanel.CreateGraphics();
            paintPanel.BackColor = defaultColor;
        }

        

        private void timer1_Tick(object sender, EventArgs e)
        {
            g.Clear(defaultColor);
            g.FillEllipse(PlayerBrush, Circle2Rectangle(playerPosition, playerRadius));
            g.FillEllipse(enemyBrush, Circle2Rectangle(enemyPosition, enemyRadius));
            if (playerBulletExist) g.FillEllipse(playerBulletBrush, Circle2Rectangle(playerBulletPosition, playerBulletRadius));
            if (enemyBulletExist) g.FillEllipse(enemyBulletBrush, Circle2Rectangle(enemyBulletPosition, enemyBulletRadius));
        }

        private void Form1_KeyPress(object sender, KeyPressEventArgs e)
        {
            switch (e.KeyChar)
            {
                case ' ':
                    if (!playerBulletExist)
                    {
                        playerBulletExist = true;
                        playerBulletPosition = playerPosition;
                    }
                    break;
                case 'w':
                    playerPosition = sumPoints(playerPosition, new Point((int)(playerMoveSpeed * Math.Cos(PlayerAngle)), (int)(playerMoveSpeed * Math.Sin(PlayerAngle))));
                    break;
                case 's':
                    playerPosition = sumPoints(playerPosition, new Point(-(int)(playerMoveSpeed * Math.Cos(PlayerAngle)), -(int)(playerMoveSpeed * Math.Sin(PlayerAngle))));
                    break;
                case 'a':
                    PlayerAngle -= playerRotateSpeed;
                    if (PlayerAngle < 0) PlayerAngle += 2 * Math.PI;
                    break;
                case 'd':
                    PlayerAngle += playerRotateSpeed;
                    if (PlayerAngle >= Math.PI) PlayerAngle -= 2 * Math.PI;
                    break;

                default:
                    break;
            }
        }
    }
}
