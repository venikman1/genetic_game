#include <cmath>

#include <iostream>

#include "player.h"

namespace gtc {
    Point::Point(double x, double y): x(x), y(y) {}

    Point Point::operator-() const {
        return Point(-x, -y);
    }
    double Point::angle() const {
        double res = std::atan2(y, x);
        if (res < 0) 
            res += 2 * PI;
        return res;
    }
    double Point::length() const {
        return std::sqrt(x * x + y * y);
    }
    void Point::normalize() {
        double len = length();
        x /= len;
        y /= len;
    }

    Point operator+(const Point& a, const Point& b) {
        return Point(a.x + b.x, a.y + b.y);
    }
    Point operator-(const Point& a, const Point& b) {
        return a + (-b);
    }
    Point operator*(const Point& a, double k) {
        return Point(a.x * k, a.y * k);
    }
    Point operator*(double k, const Point& a) {
        return a * k;
    }
    Point operator/(const Point& a, double k) {
        return a * (1.0 / k);
    }
    bool operator==(const Point& a, const Point& b) {
        return is_equal(a.x, b.x) && is_equal(a.y, b.y);
    }
    bool operator!=(const Point& a, const Point& b) {
        return !(a == b);
    }
    Point& operator+=(Point& a, const Point& b) {
        a = a + b;
        return a;
    }
    Point& operator-=(Point& a, const Point& b) {
        a = a - b;
        return a;
    }
    Point& operator*=(Point& a, double k){
        a = a * k;
        return a;
    }

    Point create_normal_vector(double angle) {
        return Point(std::cos(angle), std::sin(angle));
    }
    bool is_equal(double a, double b) {
        return std::fabs(a - b) < EPS;
    }
    double scalar_mult(const Point& a, const Point& b) {
        return a.x * b.x + a.y * b.y;
    }
    double vector_mult(const Point& a, const Point& b) {
        a.x * b.y - a.y * b.x;
    }
    double distance_from_ray_to_point(Point ray_start, Point ray_direction, Point point) {
        Point ray_end = ray_start + ray_direction;
        if (scalar_mult(ray_direction, point - ray_start) < 0)
            return (ray_start - point).length();
        double a = ray_start.y - ray_end.y;
        double b = ray_end.x - ray_start.x;
        double c = -(a * ray_start.x + b * ray_start.y);
        return std::fabs(a * point.x + b * point.y + c) / std::sqrt(a * a + b * b);
    }


    Player::Player(const Point& pos, double speed, double ang_speed, double radius): 
        position(pos),
        speed(speed),
        angle(0),
        is_bullet_fired(false),
        angular_speed(ang_speed),
        radius(radius),
        is_moving(false) {}
    void Player::forward(double time) {
        position += create_normal_vector(angle) * speed * time;
        is_moving = true;
    }
    void Player::backward(double time) {
        position += create_normal_vector(angle) * speed * time;
        is_moving = true;
    }
    void Player::left(double time) {
        angle += angular_speed * time;
        is_moving = true;

        angle = std::fmod(angle, 2*PI);
    }
    void Player::right(double time) {
        angle -= angular_speed * time;
        is_moving = true;

        angle = std::fmod(angle, 2*PI);
    }
    void Player::shoot(double time) {
        if (!is_bullet_fired) {
            // std::cerr << "BULLET FIRED\n";
            // std::cerr << "angle is " << angle * 180 / PI << "\n";
            is_bullet_fired = true;
            bullet_speed_vec = create_normal_vector(angle) * BULLET_SPEED;
            bullet = position;
        }
        is_moving = false;
    }
    void Player::nothing(double time) {
        is_moving = false;
    }
    void Player::move_bullet(double time) {
        if (is_bullet_fired) {
            bullet += bullet_speed_vec * time;
            // std::cerr << "Bullet at: " << bullet.x << " " << bullet.y << "\n";
        }
    }
    void Player::delete_bullet() {
        is_bullet_fired = false;
    }
    void Player::deal_damage(double dmg) {
        damage_taken += dmg;
    }
}
