#pragma once

#include "gen.h"
#include "player.h"

namespace gtc {
    /* genetic namespace */
    class Game {
    public:
        std::vector<Command> log1, log2;
        Player player1, player2;
        Gen gen1, gen2;
        double width, height;
        double bullet_dmg;
        double time;
        double time_delta;

        Game(double width = 600, double height = 600, double bullet_dmg = 50, double time_delta = 0.01);

        void load_gen1(Gen gen1);
        void load_gen2(Gen gen2);
        void run_game(double end_time);
        void process_player_action(Player& player1, Player& player2, Gen& gen, std::vector<Command>& log);
        bool is_out_of_bounds(const Point& point);
        Point warp(const Point& point);
        void save_logs_to_file(const char* path1, const char* path2);
    };
}
