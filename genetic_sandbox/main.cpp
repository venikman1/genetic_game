#include <iostream>
#include <vector>

#include "game.h"

int main() {
    std::cout.precision(2);
    std::cout.setf(std::ios::fixed);

    std::vector<gtc::Command> code(64);
    code[0] = gtc::IF_VISIBLE;
    code[1] = gtc::SHOOT;
    code[2] = gtc::LEFT;
    gtc::Gen gen(code);

    std::vector<gtc::Command> code2(64);
    // code2[0] = gtc::IF_VISIBLE;
    // code2[1] = 3;
    // code2[2] = gtc::RIGHT;
    // code2[3] = gtc::IF_BULLET_FIRED;
    // code2[4] = gtc::BACKWARD_LEFT;
    // code2[5] = gtc::SHOOT;
    code2[0] = gtc::IF_BULLET_HIT;
    code2[1] = gtc::BACKWARD_LEFT;
    code2[2] = gtc::IF_VISIBLE;
    code2[3] = gtc::SHOOT;
    code2[4] = gtc::RIGHT;
    gtc::Gen gen2(code2);
    gen.save_to_file("files/player1.gen");
    gen2.save_to_file("files/player2.gen");
    std::cout << "Saved genes\n";

    gtc::Game game;
    game.load_gen1(gen);
    game.load_gen2(gen2);
    game.run_game(20.0);
    game.save_logs_to_file("files/log1.log", "files/log2.log");
    std::cout << "Game ended, results:\nFirst player dealt " << game.player2.damage_taken << " damage\n"<<
        "Second player dealt " << game.player1.damage_taken << " damage\n" << "Logs saved at \"files/\"\n";
    return 0;
}
