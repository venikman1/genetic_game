#include <cmath>
#include <algorithm>
#include <fstream>
#include <iostream>

#include "game.h"

namespace gtc {
    /* genetic namespace */
    Game::Game(double width, double height, double bullet_dmg, double time_delta):
        width(width),
        height(height),
        bullet_dmg(bullet_dmg),
        time_delta(time_delta),
        time(0),
        player1(Point(100, 100)),
        player2(Point(width - 100, height - 100))
    {

    }
    void Game::load_gen1(Gen gen1) {
        this->gen1 = gen1;
    }
    void Game::load_gen2(Gen gen2) {
        this->gen2 = gen2;
    }
    void Game::run_game(double end_time) {
        bool is_moving1 = false;
        bool is_moving2 = false;
        while (time < end_time) {
            process_player_action(player1, player2, gen1, log1);
            process_player_action(player2, player1, gen2, log2);
            time += time_delta;
        }
    }
    void Game::process_player_action(Player& player1, Player& player2, Gen& gen, std::vector<Command>& log) {
        double player_distance = (player1.position - player2.position).length();
        double distance_to_bullet = (player1.position - player2.bullet).length();
        double is_bullet_hits = distance_from_ray_to_point(player2.bullet, player2.bullet_speed_vec, player1.position) <= player1.radius;
        if (!player2.is_bullet_fired) {
            distance_to_bullet = 10000; // TODO: исправить говно
            is_bullet_hits = false;
        }
        Command command = gen.run_code(player_distance,
            distance_from_ray_to_point(player1.position, create_normal_vector(player1.angle), player2.position) <= player2.radius,
            player2.is_moving,
            distance_to_bullet,
            is_bullet_hits,
            player1.is_bullet_fired
        );
        // if (distance_from_ray_to_point(player1.position, create_normal_vector(player1.angle), player2.position) <= player2.radius) {
        //     std::cerr << "FINDED ENEMY\n";
        //     std::cerr << player1.position.x << " " << player1.position.y << "\n";
        //     std::cerr << player1.angle * 180/PI << "\n";
        //     std::cerr << player2.position.x << " " << player2.position.y << "\n";
        // }
        // std::cout << distance_from_ray_to_point(player1.position, create_normal_vector(player1.angle), player2.position) << "\n";
        // std::cout << player1.position.x << " " << player1.position.y << "\n";
        // std::cout << create_normal_vector(player1.angle).x << " " << create_normal_vector(player1.angle).y << "\n";
        // std::cout << player2.position.x << " " << player2.position.y << "\n";
        // exit(0);
        log.push_back(command);
        switch (command) {
            case LEFT: {
                player1.left(time_delta);
            break;}
            case RIGHT: {
                player1.right(time_delta);
            break;}
            case FORWARD: {
                player1.forward(time_delta);
            break;}
            case BACKWARD:{
                player1.backward(time_delta);
            break;}
            case FORWARD_RIGHT: {
                player1.forward(time_delta);
                player1.right(time_delta);
            break;}
            case FORWARD_LEFT: {
                player1.forward(time_delta);
                player1.left(time_delta);
            break;}
            case BACKWARD_RIGHT: {
                player1.backward(time_delta);
                player1.right(time_delta);
            break;}
            case BACKWARD_LEFT: {
                player1.backward(time_delta);
                player1.left(time_delta);
            break;}
            case SHOOT: {
                player1.shoot(time_delta);
            break;}
            case NOTHING: {
                player1.nothing(time_delta);
            break;}
        }
        player1.move_bullet(time_delta);
        if (player1.is_bullet_fired) {
            if ((player1.bullet - player2.position).length() <= player2.radius) {
                player1.delete_bullet();
                player2.deal_damage(bullet_dmg);
                // std::cerr << "BANG\n";
            }
            if (is_out_of_bounds(player1.bullet))
                player1.delete_bullet();
        }
        player1.position = warp(player1.position);
    }
    bool Game::is_out_of_bounds(const Point& point) {
        return (
            point.x < 0 ||
            point.x > width ||
            point.y < 0 ||
            point.y > height
        );
    }
    Point Game::warp(const Point& point) {
        return Point(std::fmod(point.x, width), std::fmod(point.y, height));
    }
    void Game::save_logs_to_file(const char* path1, const char* path2) {
        std::ofstream output;
        output.open(path1, std::ios::binary | std::ios::out);
        output.write(&log1[0], log1.size());
        output.close();

        output.open(path2, std::ios::binary | std::ios::out);
        output.write(&log2[0], log2.size());
        output.close();
    }

}
