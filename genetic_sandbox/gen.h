#pragma once

#include <vector>
#include <string>
#include <random>

namespace gtc { 
    /* genetic namespace */
    using Command = char;

    const double DIST_DELIM = 100.0;
    const int GEN_SIZE = 64;
    
    const Command LEFT = 64;
    const Command RIGHT = 65;
    const Command FORWARD = 66;
    const Command BACKWARD = 67;
    const Command SHOOT = 68;
    const Command NOTHING = 69;
    const Command IF_VISIBLE = 70;
    const Command IF_ENEMY_DIST = 71;
    const Command IF_MOVING = 72;
    const Command IF_BULLET_DIST = 73;
    const Command IF_BULLET_HIT = 74;
    const Command FORWARD_RIGHT = 75;
    const Command FORWARD_LEFT = 76;
    const Command BACKWARD_RIGHT = 77;
    const Command BACKWARD_LEFT = 78;
    const Command IF_RANDOM = 79;
    const Command IF_BULLET_FIRED = 80;

    bool is_command_goto(const Command& command);
    bool is_command_terminal(const Command& command);
    bool is_command_moving(const Command& command);

    class Gen {
    private:
        std::vector<Command> code;
        std::mt19937 rnd;
    public:
        Gen();
        Gen(const std::vector<Command>& init_code);
        Gen(std::vector<Command>&& init_code);

        std::vector<Command>& get_code();
        Command run_code(
            double distance_to_enemy,
            bool is_enemy_visible,
            bool is_enemy_moving,
            double distance_to_bullet,
            bool is_bullet_hitting,
            bool is_bullet_fired
        );

        void mutate();

        void load_from_file(const char* path);
        void save_to_file(const char* path) const;
    };
}
