#pragma once

namespace gtc {
    /* genetic namespace */
    const double PI = 3.14159265358979;
    const double EPS = 1e-9;

    class Point {
    private:

    public:
        double x, y;

        Point(double x = 0, double y = 0);
        Point operator-() const;
        double angle() const;
        double length() const;
        void normalize();
    };

    Point operator+(const Point& a, const Point& b);
    Point operator-(const Point& a, const Point& b);
    Point operator*(const Point& a, double k);
    Point operator*(double k, const Point& a);
    Point operator/(const Point& a, double k);
    bool operator==(const Point& a, const Point& b);
    bool operator!=(const Point& a, const Point& b);
    Point& operator+=(Point& a, const Point& b);
    Point& operator-=(Point& a, const Point& b);
    Point& operator*=(Point& a, double k);

    Point create_normal_vector(double angle);
    bool is_equal(double a, double b);
    double scalar_mult(const Point& a, const Point& b);
    double vector_mult(const Point& a, const Point& b);
    double distance_from_ray_to_point(Point ray_start, Point ray_direction, Point point);

    const double PLAYER_SPEED = 100;
    const double BULLET_SPEED = 100;
    const double PLAYER_ANGULAR_SPEED = PI;
    const double PLAYER_RADIUS = 25;

    class Player {
    public:
        Point position;
        Point bullet, bullet_speed_vec;
        double speed;
        double angle;
        double angular_speed;
        double radius;
        bool is_bullet_fired;
        double damage_taken;
        bool is_moving;

        Player(const Point& pos, double speed = PLAYER_SPEED, double ang_speed = PLAYER_ANGULAR_SPEED, double radius = PLAYER_RADIUS);
        void forward(double time);
        void backward(double time);
        void left(double time);
        void right(double time);
        void shoot(double time);
        void nothing(double time);
        void move_bullet(double time);
        void delete_bullet();
        void deal_damage(double dmg);
    };
}
