#include <cmath>
#include <algorithm>
#include <fstream>

#include "gen.h"

namespace gtc { 
    /* genetic namespace */
    bool is_command_goto(const Command& command) {
        return command < 64;
    }
    bool is_command_terminal(const Command& command) {
        return (
            command == LEFT ||
            command == RIGHT ||
            command == FORWARD ||
            command == BACKWARD ||
            command == FORWARD_RIGHT ||
            command == FORWARD_LEFT ||
            command == BACKWARD_RIGHT ||
            command == BACKWARD_LEFT ||
            command == SHOOT ||
            command == NOTHING
        );
    }
    bool is_command_moving(const Command& command) {
        return (
            command == LEFT ||
            command == RIGHT ||
            command == FORWARD ||
            command == BACKWARD ||
            command == FORWARD_RIGHT ||
            command == FORWARD_LEFT ||
            command == BACKWARD_RIGHT ||
            command == BACKWARD_LEFT
        );
    }

    Gen::Gen() {
        code.resize(GEN_SIZE);
    }
    Gen::Gen(const std::vector<Command>& init_code): code(init_code) {}
    Gen::Gen(std::vector<Command>&& init_code): code(init_code) {}

    std::vector<Command>& Gen::get_code() {
        return code;
    }
    Command Gen::run_code(
        double distance_to_enemy,
        bool is_enemy_visible,
        bool is_enemy_moving,
        double distance_to_bullet,
        bool is_bullet_hitting,
        bool is_bullet_fired
    ) {
        int pointer = 0;
        for (int counter = 0; counter < GEN_SIZE; ++counter) {
            Command& command = code[pointer];
            if (is_command_terminal(command))
                return command;
            if (is_command_goto(command))
                pointer = command; // Ууххххх
            switch (command) {
                case IF_VISIBLE: {
                    if (is_enemy_visible)
                        pointer += 1;
                    else
                        pointer += 2;
                break;}
                case IF_ENEMY_DIST: {
                    pointer += std::min(4, static_cast<int>(std::floor(distance_to_enemy/DIST_DELIM))) + 1;
                break;}
                case IF_MOVING: {
                    if (is_enemy_moving)
                        pointer += 1;
                    else
                        pointer += 2;
                break;}
                case IF_BULLET_DIST: {
                    pointer += std::min(4, static_cast<int>(std::floor(distance_to_bullet/DIST_DELIM))) + 1;
                break;}
                case IF_BULLET_HIT: {
                    if (is_bullet_hitting)
                        pointer += 1;
                    else
                        pointer += 2;
                break;}
                case IF_RANDOM: {
                    if (rnd() % 2 == 0)
                        pointer += 1;
                    else
                        pointer += 2;
                break;}
                case IF_BULLET_FIRED: {
                    if (is_bullet_fired)
                        pointer += 1;
                    else
                        pointer += 2;
                break;}
            }
            pointer %= GEN_SIZE;
        }
        return NOTHING;
    }
    void Gen::mutate() {
        int pointer = rnd() % GEN_SIZE;
        int new_code = rnd() % 81; // TODO: поставить константу
        code[pointer] = new_code;
    }
    void Gen::load_from_file(const char* path) {
        std::ifstream input;
        input.open(path, std::ios::binary | std::ios::in);
        input.read(&code[0], 64);
        input.close();
    }
    void Gen::save_to_file(const char* path) const {
        std::ofstream output;
        output.open(path, std::ios::binary | std::ios::out);
        output.write(&code[0], 64);
        output.close();
    }
}
